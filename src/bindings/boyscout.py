
import cffi
import cffi.verifier
import os

cffi.verifier.cleanup_tmpdir()
ffi = cffi.FFI()
ffi.cdef("""
typedef void (*HashTableEntryDelete)(void *ptr);

struct ht_entry {
    char *key;
    void *value;
    HashTableEntryDelete release;
};

struct ht {
    struct ht_entry **entries;
    size_t count;
    size_t size;
    double resize_at;
};

typedef struct ht_entry HashTableEntry;
typedef struct ht HashTable;

HashTable *htable_new();
HashTable *htable_new_with(size_t buckets, float resize_at);
void htable_delete(HashTable *ht);
bool htable_put(HashTable *ht, const char *key, void *value, HashTableEntryDelete fn);
HashTableEntry* htable_get(HashTable *ht, const char *key);
void htable_delete_entry(HashTable *ht, const char *key);
void htable_enumerate(HashTable *ht, void *data, bool (*callback)(HashTableEntry *entry, size_t idx, void *data));


typedef void (*bs_backend_destructor)(void *handle);
typedef void *(*bs_backend_open)(const char *path);
typedef int (*bs_backend_close)(void *handle);
typedef int (*bs_backend_iterate)(void *handle, void *user, int (*callback)(void *, const char *path));
typedef int (*bs_backend_copy)(void *handle, const char *src, const char *dest);
typedef int (*bs_backend_load)(void *handle, const char *path, uint8_t **data, size_t *size);
typedef int (*bs_backend_mode)(void *handle, const char *path, int64_t *out);
typedef int (*bs_backend_size)(void *handle, const char *path, int64_t *out);
typedef int (*bs_backend_atime)(void *handle, const char *path, int64_t *out);
typedef int (*bs_backend_mtime)(void *handle, const char *path, int64_t *out);
typedef int (*bs_backend_ctime)(void *handle, const char *path, int64_t *out);
typedef int (*bs_backend_uid)(void *handle, const char *path, int64_t *out);
typedef int (*bs_backend_gid)(void *handle, const char *path, int64_t *out);
typedef int (*bs_backend_exists)(void *handle, const char *path, bool *out);

typedef struct bs_backend {
    char *name;
    void *handle;
    bs_backend_destructor destructor;
    bs_backend_open open_driver;
    bs_backend_close close_driver;
    bs_backend_iterate iterate;
    bs_backend_copy copy_file;
    bs_backend_load load_file;
    bs_backend_mode mode;
    bs_backend_size size;
    bs_backend_atime atime;
    bs_backend_mtime mtime;
    bs_backend_ctime ctime;
    bs_backend_uid uid;
    bs_backend_gid gid;
    bs_backend_exists exists;
}
BS_Backend;

typedef struct bs  {
    HashTable *plugins;
    HashTable *backends;
    HashTable *filetype_mappings;
    HashTable *commands;
    void *database;
}
BS;

typedef struct bs_file_context
{
    char *path;
    void *handle;
    void *driver;
}
BS_File;

typedef struct bs_command {
    int (*command)(BS *b, int argc, char *argv[]);
    char *name;
    char *help;
} BS_Command;

typedef int (*bs_plugin_init)(void *context);

BS* bs_init();
void bs_destroy(BS* b);
int bs_supported_files(BS *b);
int bs_plugin_register(BS *b, const char *name, const char *path);
void bs_plugin_discover(BS *b, const char *plugin_dir);
int bs_backend_register(BS *b,
        const char *name,
        void *handle,
        bs_backend_destructor destructor,
        bs_backend_open open_driver,
        bs_backend_close close_driver,
        bs_backend_iterate iterate,
        bs_backend_copy copy_file,
        bs_backend_load load_file,
        bs_backend_mode mode,
        bs_backend_size size,
        bs_backend_atime atime,
        bs_backend_mtime mtime,
        bs_backend_ctime ctime,
        bs_backend_uid uid,
        bs_backend_gid gid,
        bs_backend_exists exists,
        ...);

int bs_command_register(BS *b,
                        const char *name,
                        int (*command)(BS *, int, char **),
                        const char *help);

int bs_database_prepared(BS *b,
    int (*callback)(void*,int,char**,char**),
    void *user_data,
    const char *sql,
    const char *bind_fmt,
    ...);

int bs_database_exec(BS *b,
    int (*callback)(void *, int, char **, char **),
    void *user_data,
    const char *sql);

int bs_open(BS *b, const char *path, BS_File **f);
int bs_close(BS *b, BS_File *f);
int bs_iterate(BS *b, BS_File *f, void *user, int (*callback)(void *, const char *));

int bs_file_copy(BS *b, BS_File *f, const char *src, const char *dest);
int bs_file_load(BS *b, BS_File *f, const char *src, uint8_t **dest, int64_t *size);
int bs_file_mode(BS *b, BS_File *f, const char *path, int64_t *out);
int bs_file_size(BS *b, BS_File *f, const char *path, int64_t *out);
int bs_file_atime(BS *b, BS_File *f, const char *path, int64_t *out);
int bs_file_mtime(BS *b, BS_File *f, const char *path, int64_t *out);
int bs_file_ctime(BS *b, BS_File *f, const char *path, int64_t *out);
int bs_file_uid(BS *b, BS_File *f, const char *path, int64_t *out);
int bs_file_gid(BS *b, BS_File *f, const char *path, int64_t *out);
int bs_file_exists(BS *b, BS_File *f, const char *path, bool *out);

void vdi_driver_register(BS *b);
""")

def main():

    BS = ffi.dlopen(os.getenv("BOYSCOUT_LIB", "libboyscout.so"))
    b = BS.bs_init()
    BS.vdi_driver_register(b);
    BS.bs_supported_files(b)

    fptr = ffi.new("BS_File **f")
    rc = BS.bs_open(b, "test.qcow2", fptr)
    print(rc)

    f = fptr[0]
    if f != ffi.NULL:
        print('Initialization seems to work..')
        BS.bs_close(b, f)
    BS.bs_destroy(b)

if __name__ == "__main__":
    main()
