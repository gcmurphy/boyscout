#include <gtest/gtest.h>
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <unistd.h>
#include "boyscout.h"
#include "utils.h"

TEST(utils, test_endswith){
    ASSERT_TRUE(bs_endswith("foo.tar.gz", ".gz"));
    ASSERT_TRUE(bs_endswith("foo.tar.gz", "tar.gz"));
    ASSERT_FALSE(bs_endswith("foo.tar.gz", "bz"));
    ASSERT_FALSE(bs_endswith("foo", "foood"));
}

TEST(utils, test_rmdir_empty){
    struct stat info;
    char tmpdir[] = ".testXXXXXX";
    mkdtemp(tmpdir);
    bs_rmdir(tmpdir);
    ASSERT_FALSE(stat(tmpdir, &info) == 0);
}
