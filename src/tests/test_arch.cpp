#include <gtest/gtest.h>
#include "boyscout.h"
#include "utils.h"
#include "drivers/arch.h"

static char TEST_TARGZ_IMAGE[] = "testdata/img.tar.gz";
static char TEST_ISO_IMAGE[] = "testdata/img.iso";
static char TEST_JAR_IMAGE[] = "testdata/img.jar";
static char TEST_WHL_IMAGE[] = "testdata/img.whl";

TEST(arch_driver, test_init){
    BS *b = bs_init();
    arch_driver_register(b);
    #ifdef DEBUG
    bs_supported_files(b);
    #endif
    BS_File *f = NULL;
    int rc = bs_open(b, TEST_TARGZ_IMAGE, &f);
    EXPECT_EQ(rc, BS_OK);
    ASSERT_TRUE(f != NULL);
    bs_close(b, f);
    bs_destroy(b);
}

static int iterate_callback(void *p, const char *path){
    int64_t *count = (int64_t*) p;
    *count = *count + 1;
    //puts(path);
    return BS_OK;
}

TEST(arch_driver, test_iterate){
    BS *b = bs_init();
    arch_driver_register(b);

    BS_File *f = NULL;
    int rc = bs_open(b, TEST_TARGZ_IMAGE, &f);
    EXPECT_EQ(rc, BS_OK);
    ASSERT_TRUE(f != NULL);
    if (rc == BS_OK){
        int64_t count = 0;
        bs_iterate(b, f, &count, "/", iterate_callback);
        ASSERT_TRUE(count > 0);
        bs_close(b, f);
    }
    bs_destroy(b);
}

TEST(arch_driver, test_stat){
    BS *b = bs_init();
    arch_driver_register(b);

    BS_File *f = NULL;
    int rc = bs_open(b, TEST_TARGZ_IMAGE, &f);
    EXPECT_EQ(rc, BS_OK);
    ASSERT_TRUE(f != NULL);
    if (rc == BS_OK){
        BS_Stat fileinfo;
        rc = bs_file_stat(b, f, "tarsnap-autoconf-1.0.35/configure", &fileinfo);
        EXPECT_EQ(rc, BS_OK);
        bs_close(b, f);
    }
    bs_destroy(b);
}
