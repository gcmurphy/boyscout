#if 0
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "chain.h"

bool string_compare(void *a, void *b){
    const char *astr = (const char *) a;
    const char *bstr = (const char *) b;
    return strcmp(astr, bstr) == 0;
}

bool string_visitor(Link *lnk, void *user){
    const char *s = (const char *) lnk->value;
    int *index = (int *) user;
    (*index)++;
    return true;
}

void test_empty_chain(){
    printf("[%s]\n", __func__);
    int count = 0;
    Chain *c = chain_new();

    assert(chain_pop_back(c) == NULL);
    assert(chain_pop_front(c) == NULL);
    assert(chain_find(c, string_compare, "apples") == NULL);
    assert(chain_size(c) == 0);

    chain_iter_forward(c, string_visitor, &count);
    assert(count == 0);

    chain_iter_back(c, string_visitor, &count);
    assert(count == 0);

    chain_delete(c);
    printf("\nPASS\n\n");
}

void test_one_link_chain_back(){
    printf("[%s]\n", __func__);

    int count = 0;
    Chain *c = chain_new();
    const char *valid = "something";
    const char *invalid = "nothing";

    chain_push_back(c, (void*)valid, NULL);

    assert(chain_size(c) == 1);

    chain_iter_back(c, string_visitor, &count);
    assert(count == 1);

    assert(chain_find(c, string_compare, (void*) invalid) == NULL);

    Link *ptr = chain_find(c, string_compare, (void *) valid);
    assert(ptr != NULL);

    const char *val = (const char *) ptr->value;
    assert(strcmp(val, valid) == 0);

    assert(chain_pop_back(c) != NULL);
    assert(chain_size(c) == 0);
    chain_delete(c);
    printf("\nPASS\n\n");
}

void test_one_link_chain_front(){
    printf("[%s]\n", __func__);
    int count = 0;
    Chain *c = chain_new();
    const char *valid = "something";
    const char *invalid = "nothing";

    chain_push_front(c, (void*)valid, NULL);

    assert(chain_size(c) == 1);

    chain_iter_forward(c, string_visitor, &count);
    assert(count == 1);

    assert(chain_find(c, string_compare, (void*) invalid) == NULL);

    Link *ptr = chain_find(c, string_compare, (void *) valid);
    assert(ptr != NULL);

    const char *val = (const char *) ptr->value;
    assert(strcmp(val, valid) == 0);

    assert(chain_pop_front(c) != NULL);
    assert(chain_size(c) == 0);
    chain_delete(c);
    printf("\nPASS\n\n");
}

void test_one_link_chain_unlink(){
    printf("[%s]\n", __func__);
    int count = 0;
    Chain *c = chain_new();
    chain_push_back(c, (void*) strdup("foo"), free);
    assert(chain_size(c) == 1);
    Link *lnk = chain_find(c, string_compare, (void *) "foo");
    assert(lnk != NULL);

    chain_unlink(c, lnk);
    assert(chain_size(c) == 0);
    assert(c->head == NULL);
    assert(c->tail == NULL);
    chain_delete(c);
    printf("\nPASS\n\n");
}

void test_chain_free(){
    int count = 0;
    Chain *c = chain_new();
    chain_push_back(c, (void*) strdup("foo"), free);
    chain_push_back(c, (void*) strdup("bar"), free);
    chain_push_back(c, (void*) strdup("baz"), free);
    chain_delete(c);
    printf("%s - ok\n", __func__);
}

void test_two_link_chain_back(){
    printf("[%s]\n", __func__);

    char *str = NULL;
    int count = 0;
    Chain *c = chain_new();
    chain_push_back(c, (void *) strdup("foo"), free);
    chain_push_back(c, (void *) strdup("bar"), free);
    assert(chain_size(c) == 2);
    chain_iter_back(c, string_visitor, &count);
    printf("count = %d\n", count);
    assert(count == 2);
    assert(chain_find(c, string_compare, (void*) "blah") == NULL);
    assert(chain_find(c, string_compare, (void *) "foo") != NULL);
    assert(chain_find(c, string_compare, (void *) "bar") != NULL);

    str = chain_pop_back(c);
    assert(str != NULL);
    assert(strcmp("bar", str) == 0);
    free(str);

    str = chain_pop_back(c);
    assert(str != NULL);
    assert(strcmp("foo", str) == 0);
    free(str);

    assert(chain_size(c) == 0);
    chain_delete(c);
    printf("\nPASS\n\n");
}

void test_two_link_chain_front(){
    printf("[%s]\n", __func__);

    char *str = NULL;
    int count = 0;
    Chain *c = chain_new();
    chain_push_front(c, (void *) strdup("foo"), free);
    chain_push_front(c, (void *) strdup("bar"), free);
    assert(chain_size(c) == 2);
    chain_iter_forward(c, string_visitor, &count);
    printf("count = %d\n", count);
    assert(count == 2);
    assert(chain_find(c, string_compare, (void*) "blah") == NULL);
    assert(chain_find(c, string_compare, (void *) "foo") != NULL);
    assert(chain_find(c, string_compare, (void *) "bar") != NULL);

    str = chain_pop_front(c);
    assert(str != NULL);
    assert(strcmp("bar", str) == 0);
    free(str);

    str = chain_pop_front(c);
    assert(str != NULL);
    assert(strcmp("foo", str) == 0);
    free(str);

    assert(chain_size(c) == 0);
    chain_delete(c);
    printf("\nPASS\n\n");
}

void test_multi_link_chain_unlink(){
    printf("[%s]\n", __func__);
    int count = 0;
    Chain *c = chain_new();
    chain_push_back(c, (void*) strdup("foo"), free);
    chain_push_back(c, (void*) strdup("bar"), free);
    chain_push_back(c, (void*) strdup("baz"), free);
    chain_push_back(c, (void*) strdup("rot"), free);
    assert(chain_size(c) == 1);
    Link *lnk = chain_find(c, string_compare, (void *) "baz");
    assert(lnk != NULL);

    chain_unlink(c, lnk);
    assert(chain_size(c) == 3);
    assert(c->head != NULL);
    assert(c->tail != NULL);
    chain_delete(c);
    printf("\nPASS\n\n");

}

void test_multi_link_chain(){
  printf("[%s]\n", __func__);

    char *str = NULL;
    int count = 0;
    Chain *c = chain_new();
    chain_push_back(c, (void *) strdup("foo"), free);
    chain_push_back(c, (void *) strdup("bar"), free);
    chain_push_back(c, (void *) strdup("baz"), free);
    assert(chain_size(c) == 3);
    chain_iter_forward(c, string_visitor, &count);
    printf("count = %d\n", count);
    assert(count == 3);
    assert(chain_find(c, string_compare, (void*) "blah") == NULL);
    assert(chain_find(c, string_compare, (void *) "foo") != NULL);
    assert(chain_find(c, string_compare, (void *) "bar") != NULL);
    assert(chain_find(c, string_compare, (void *) "baz") != NULL);

    Link *lnk = chain_find(c, string_compare, (void*) "bar");
    chain_unlink(c, lnk);

    str = chain_pop_back(c);
    assert(str != NULL);
    puts(str);
    assert(strcmp("baz", str) == 0);
    free(str);

    str = chain_pop_back(c);
    assert(str != NULL);
    assert(strcmp("foo", str) == 0);
    free(str);

    assert(chain_size(c) == 0);
    chain_delete(c);
    printf("\nPASS\n\n");

}


int main(){
    test_empty_chain();
    test_one_link_chain_back();
    test_one_link_chain_front();
    test_one_link_chain_unlink();
    test_two_link_chain_back();
    test_two_link_chain_front();
    test_chain_free();
    test_multi_link_chain();
    return 0;
}
#endif
