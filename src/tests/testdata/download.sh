#!/usr/bin/bash

# qcow2 file
if [ ! -e "img.qcow2" ]; then
    wget -O img.qcow2 http://download.cirros-cloud.net/0.3.4/cirros-0.3.4-i386-disk.img
fi

# .tar.gz file
if [ ! -e "img.tar.gz" ]; then
    wget -O img.tar.gz https://www.tarsnap.com/download/tarsnap-autoconf-1.0.35.tgz
fi

# jar file
if [ ! -e "img.jar" ]; then
    wget -O img.jar http://search.maven.org/remotecontent?filepath=org/apache/struts/struts2-osgi-demo-bundle/2.3.24/struts2-osgi-demo-bundle-2.3.24.jar
fi

# iso file
if [ ! -e "img.iso" ]; then
    wget -O img.iso ftp://distro.ibiblio.org/pub/linux/distributions/damnsmall/dslcore/dslcore_20080630.iso
fi

# whl file
if [ ! -e "img.whl" ]; then
    wget -O img.whl https://pypi.python.org/packages/py2/b/bandit/bandit-0.12.0-py2-none-any.whl
fi
