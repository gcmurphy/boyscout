#include <gtest/gtest.h>
#include <dirent.h>
#include <limits.h>
#include "boyscout.h"
#include "utils.h"
#include "drivers/arch.h"
#include "drivers/virtdisk.h"

static char TESTDATA_DIR[] = "testdata";

static int iterate_callback(void *p, const char *path){
    int64_t *count = (int64_t*) p;
    *count = *count + 1;
    //puts(path);
    return BS_OK;
}

TEST(format, format_readable){
    struct dirent *dp;
    int rc;
    int64_t count;
    char path[PATH_MAX];
    BS_File *f;
    BS *b = bs_init();
    arch_driver_register(b);
    vdi_driver_register(b);
    DIR *dirp = opendir(TESTDATA_DIR);
    while ((dp = readdir(dirp)) != NULL){
        if (strcmp(".", dp->d_name) == 0 ||
            strcmp("..", dp->d_name) == 0||
            strcmp("download.sh", dp->d_name) == 0) {
            continue;
        }
        snprintf(path, PATH_MAX-1, "%s/%s", TESTDATA_DIR, dp->d_name);
        f = NULL;
        rc = bs_open(b, path, &f);
        EXPECT_EQ(rc, BS_OK);
        ASSERT_TRUE(f != NULL);
        count = 0;
        bs_iterate(b, f, &count, "/", iterate_callback);
        ASSERT_TRUE(count > 0);
        bs_close(b, f);
    }
    closedir(dirp);
    bs_destroy(b);
}
