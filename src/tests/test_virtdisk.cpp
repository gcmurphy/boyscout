#include <gtest/gtest.h>
#include "boyscout.h"
#include "utils.h"
#include "drivers/virtdisk.h"

static char TEST_QCOW2_IMAGE[] = "testdata/img.qcow2";

TEST(vdi_driver, test_init){
    BS *b = bs_init();
    vdi_driver_register(b);
    #ifdef DEBUG
    bs_supported_files(b);
    #endif
    BS_File *f = NULL;
    int rc = bs_open(b, TEST_QCOW2_IMAGE, &f);
    EXPECT_EQ(rc, BS_OK);
    ASSERT_TRUE(f != NULL);
    if (rc == BS_OK){
        bs_close(b, f);
    }
    bs_destroy(b);
}

static int iterate_callback(void *p, const char *path){
    int64_t *count = (int64_t*) p;
    *count = *count + 1;
    return BS_OK;
}

TEST(vdidriver, test_iterate){
    BS *b = bs_init();
    vdi_driver_register(b);

    BS_File *f = NULL;
    int rc = bs_open(b, TEST_QCOW2_IMAGE, &f);
    EXPECT_EQ(rc, BS_OK);
    ASSERT_TRUE(f != NULL);
    if (rc == BS_OK){
        int64_t count = 0;
        bs_iterate(b, f, &count, "/", iterate_callback);
        ASSERT_TRUE(count > 0);
        bs_close(b, f);
    }
    bs_destroy(b);
}
