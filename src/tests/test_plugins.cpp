#include <gtest/gtest.h>
#ifdef __cplusplus
extern "C"{
#endif
#include "boyscout.h"
#include "utils.h"
#include "drivers/virtdisk.h"
#ifdef __cplusplus
}
#endif
#include <stdio.h>
#include <stdlib.h>

int sqlcallback(void *handle, int argc, char **argv, char **cols)
{
    int i;
    for (i = 0; i < argc; ++i){
        #ifdef DEBUG
        printf("%20s => %s\n", cols[i], argv[i]);
        #endif

    }
    return 0;
}

TEST(plugins, fileinfo){
    setenv("BOYSCOUT_PLUGINS", "../build/plugins", 1);
    BS *b = bs_init();
    vdi_driver_register(b);

    HashTableEntry *entry = htable_get(b->commands, "fileinfo");
    ASSERT_TRUE(entry != NULL);

    BS_Command *cmd;
    cmd = (BS_Command *)entry->value;

    ASSERT_TRUE(strcmp("fileinfo", cmd->name) == 0);
    const char *args[] = { "testdata/img.qcow2" };
    int rc = cmd->command(b, 1, args);

    EXPECT_EQ(BS_OK, rc);

    bs_database_exec(b, sqlcallback, NULL, "select * from file_info");
    bs_destroy(b);
}
