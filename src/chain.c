#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "chain.h"

Chain *
chain_new()
{
    Chain *c = calloc(1, sizeof(Chain));
    c->head = NULL;
    c->tail = NULL;
    c->count = 0;
    return c;
}

void
chain_delete(Chain *chain)
{
    Link *ptr = chain->head, *next = NULL;
    while (ptr){
        next = ptr->next;
        if (ptr->release){
            ptr->release(ptr->value);
        }
        free(ptr);
        ptr = next;
    }
    free(chain);
}

void
chain_push_back(Chain *chain, void *value, LinkValueDelete release)
{
    Link *lnk = calloc(1, sizeof(Link));
    lnk->value = value;
    lnk->release = release;

    if (chain->tail == NULL){
        chain->head = lnk;
        chain->tail = lnk;
    } else {
        chain->tail->next = lnk;
        lnk->prev = chain->tail;
        chain->tail = lnk;
    }
    chain->count++;
}

void
chain_push_front(Chain *chain, void *value, LinkValueDelete release)
{
    Link *lnk = calloc(1, sizeof(Link));
    lnk->value = value;
    lnk->release = release;
    if (chain->head == NULL){
        chain->tail = lnk;
        chain->head = lnk;
    } else {
        chain->head->prev = lnk;
        lnk->next = chain->head;
        chain->head = lnk;
    }
    chain->count ++;
}

void*
chain_pop_back(Chain *chain)
{
    void *rv;
    Link *tmp;

    /* empty */
    if (chain->tail == NULL){
        rv = NULL;

    /* 1 item */
    } else if (chain->tail == chain->head){
        rv = chain->tail->value;
        free(chain->tail);
        chain->head = NULL;
        chain->tail = NULL;
        chain->count = 0;

    /* n items */
    } else {
        Link *tmp = chain->tail;
        rv = chain->tail->value;
        chain->tail->prev->next = NULL;
        chain->count--;
        chain->tail = chain->tail->prev;
        free(tmp);
    }
    return rv;
}

void*
chain_pop_front(Chain *chain)
{
    void *rv;
    Link *tmp;

    /* empty */
    if (chain->head == NULL){
        rv = NULL;

    /* 1 item */
    } else if (chain->head == chain->tail){
        rv = chain->head->value;
        free(chain->head);
        chain->head = NULL;
        chain->tail = NULL;
        chain->count = 0;

    /* n items */
    } else {
        Link *tmp = chain->head;
        rv = chain->head->value;
        chain->head->next->prev = NULL;
        chain->count--;
        chain->head = chain->head->next;
        free(tmp);
    }
    return rv;
}

size_t chain_size(Chain *chain)
{
    return chain->count;
}

Link *
chain_find(Chain *chain, LinkComparator cmp, void *user)
{
    Link *lnk = NULL;

    lnk = chain->head;
    while (lnk){
        if (cmp(lnk->value, user)){
            return lnk;
        }
        lnk = lnk->next;
    }
    return NULL;
}

Link *
chain_iter_forward(Chain *chain, LinkVisitor visitor, void *user)
{

    Link *lnk;

    lnk = chain->head;
    while (lnk){
        if (! visitor(lnk, user)){
            break;
        }
        lnk = lnk->next;
    }
    return lnk;
}

Link *
chain_iter_back(Chain *chain, LinkVisitor visitor, void *user)
{
    Link *lnk = NULL;
    lnk = chain->tail;
    while (lnk){
        if (! visitor(lnk, user)){
            break;
        }
        lnk = lnk->prev;
    }
    return lnk;
}

void
chain_unlink(Chain *chain, Link *link)
{
    if (chain->count > 1){
        if (link->next && link->prev){
            link->next->prev = link->prev;
            link->prev->next = link->next;
        } else if (link == chain->tail){
            chain->tail = link->prev;
        } else {
            chain->head = link->next;
        }

    /* reset count */
    } else {
        chain->head = NULL;
        chain->tail = NULL;
        chain->count = 0;
    }
    /* delete link */
    if (link->release){
        link->release(link->value);
    }
    free(link);
    link = NULL;
}
