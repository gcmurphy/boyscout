#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#include "ht.h"

static HashTableEntry EmptyEntry = { .key = NULL, .value = NULL };
static const HashTableEntry * const DeleteMeEntry = &EmptyEntry;

static size_t _h(const char *key, size_t lim){
    size_t h = 0;
    size_t i = 0;

    for (i = 0; i < strlen(key); ++i){
        h += (int)key[i];
    }
    return h % lim;
}

static size_t hash(const char * key, size_t lim, size_t probe){
    return ( _h(key, lim) + probe * (_h(key, 97) + 1) ) % lim;
}

HashTable *htable_new()
{
    return htable_new_with(64, 0.75);
}
HashTable *htable_new_with(size_t buckets, float resize_at)
{
    size_t i;
    HashTable *h = malloc(sizeof(HashTable));
    h->size = buckets;
    h->count = 0;
    h->resize_at = resize_at;
    h->entries= malloc(buckets * sizeof(HashTableEntry*));
    for (i = 0; i < buckets; ++i){
        h->entries[i] = NULL;
    }
    return h;
}

void htable_delete(HashTable *ht){
    size_t i;
    HashTableEntry *p;

    p = NULL;
    for (i = 0; i < ht->size; ++i){
        p = ht->entries[i];
        if (p != NULL && p != DeleteMeEntry){
            free(p->key);
            if (p->release){
                p->release(p->value);
            }
            free(p);
        }
    }
    free(ht->entries);
    free(ht);
    ht = NULL;
}

static bool htable_grow(HashTable *ht){

    int probe, h;
    HashTableEntry **newbucket;
    HashTableEntry *oldptr;

    size_t i;
    size_t oldsize = ht->size;
    size_t newsize = 2 * ht->size;
    newbucket =  malloc(newsize * sizeof(HashTableEntry*));

    // init
    for (i = 0; i < newsize; ++i){
        newbucket[i] = NULL;
    }

    // rehash
    for (i = 0; i < oldsize; ++i){
        oldptr = ht->entries[i];
        if (oldptr && oldptr != DeleteMeEntry){
            probe = 1;
            h = hash(oldptr->key, ht->size, probe);
            while (ht->entries[h] && ht->entries[h] != DeleteMeEntry){
                probe += 1;
                h = hash(oldptr->key, ht->size, probe);
            }
            newbucket[h] = oldptr;
            ht->entries[i] = NULL;
        }
    }

    free(ht->entries);
    ht->entries = newbucket;
    ht->size = newsize;
    return true;
}

bool htable_put(HashTable *ht, const char *key, void *value, HashTableEntryDelete fn){

    size_t probe;
    size_t h;
    HashTableEntry *entry;

    if (ht->count > (size_t)(ht->resize_at * ht->size)){
        if (!htable_grow(ht)){
            return false;
        }
    }

    probe = 1;
    h = hash(key, ht->size, probe);
    while (ht->entries[h] && ht->entries[h] != DeleteMeEntry){
        probe += 1;
        h = hash(key, ht->size, probe);
    }

    entry = malloc(sizeof(HashTableEntry));
    entry->key = strdup(key);
    entry->value = value;
    entry->release = fn;
    ht->entries[h] = entry;
    ht->count += 1;

    return true;
}

HashTableEntry* htable_get(HashTable *ht, const char *key){

    size_t probe;
    size_t h;
    HashTableEntry *entry;

    probe = 0;
    while (true) {
        probe += 1;
        h = hash(key, ht->size, probe);
        entry = ht->entries[h];
        if (entry == NULL){
            break;
        }
        if (entry != DeleteMeEntry && strcmp(key, entry->key) == 0){
            return ht->entries[h];
        }
    }
    return NULL;
}

void htable_delete_entry(HashTable *ht, const char *key){

    HashTableEntry *entry = htable_get(ht, key);
    if (entry){
        free(entry->key);
        if (entry->release){
            entry->release(entry->value);
        }
        free(entry);
        entry = &EmptyEntry;
    }
}

void htable_enumerate(HashTable *ht, void *data, bool (*callback)(HashTableEntry *entry, size_t idx, void *data)){
    size_t i;
    for (i = 0; i < ht->size; ++i){
        if (ht->entries[i] && ht->entries[i] != DeleteMeEntry){
            if(! callback(ht->entries[i], i, data)){
                break;
            }
        }
    }
}

