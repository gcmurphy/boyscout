#ifndef ht_h
#define ht_h

#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef void (*HashTableEntryDelete)(void *ptr);

struct ht_entry {
    char *key;
    void *value;
    HashTableEntryDelete release;
};

struct ht {
    struct ht_entry **entries;
    size_t count;
    size_t size;
    double resize_at;
};

typedef struct ht_entry HashTableEntry;
typedef struct ht HashTable;

HashTable *htable_new();
HashTable *htable_new_with(size_t buckets, float resize_at);
void htable_delete(HashTable *ht);
bool htable_put(HashTable *ht, const char *key, void *value, HashTableEntryDelete fn);
HashTableEntry* htable_get(HashTable *ht, const char *key);
void htable_delete_entry(HashTable *ht, const char *key);
void htable_enumerate(HashTable *ht, void *data, bool (*callback)(HashTableEntry *entry, size_t idx, void *data));


#ifdef __cplusplus
};
#endif


#endif /* ht_h */
