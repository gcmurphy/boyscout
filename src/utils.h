#ifndef bs_utils_h
#define bs_utils_h

#ifdef __cplusplus
extern "C" {
#endif


#include <stdbool.h>

char *bs_hash(const char *data, size_t size);
bool bs_startswith(const char *str, const char *prefix);
bool bs_endswith(const char *str, const char *suffix);
const char *bs_file_extension(const char *path);
void  bs_rmdir(const char *path);
const char *bs_get_home_directory();
const char *bs_get_editor();
char* bs_read_null_string(FILE *f, char *out);
char * bs_permission_string(int64_t mode);
bool bs_local_file_exists(const char *path);


#ifdef __cplusplus
};
#endif

#endif /* bs_utils_h */
