#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <archive.h>
#include <archive_entry.h>
#include <sys/stat.h>
#include <limits.h>
#include "utils.h"
#include "drivers/arch.h"

void
arch_driver_register(BS *b)
{
    bs_backend_register(b,
        "archive",
        NULL,
        arch_driver_free,
        arch_driver_open,
        arch_driver_close,
        arch_iterate,
        arch_copy,
        arch_load,
        arch_exists,
        arch_stat,
        arch_extract,
        ".tar", ".zip", ".jar", ".egg", ".whl", ".rpm", /* deb */
        ".tar.gz", ".tgz", ".tar.xz", ".txz", ".iso",
        ".cab", ".rar", ".tar.bz", ".tbz", NULL);
}

void
arch_driver_free(void *handle)
{
    if (handle) free(handle);
    handle = NULL;
}

static struct archive *
open_archive(const char *path)
{
    struct archive *a;
    a = archive_read_new();
    archive_read_support_filter_all(a);
    archive_read_support_format_all(a);
    archive_read_open_filename(a, path, 10240);
    return a;
}

static void
close_archive(struct archive *a)
{
    archive_read_free(a);
    a = NULL;
}

static void
seek_to_path(struct archive *a, struct archive_entry **entry, const char *path)
{
    while (archive_read_next_header(a, entry) == ARCHIVE_OK){
        if (strcmp(path, archive_entry_pathname(*entry)) == 0){
            return;
        }
    }
    *entry = NULL;
}

void *
arch_driver_open(void *handle, const char *path)
{
    return strdup(path);
}

int
arch_driver_close(void *handle)
{
    free(handle);
    return BS_OK;
}

int
arch_iterate(void *handle,
    void *user,
    const char *dir,
    int (*callback)(void *, const char *))
{
    const char *path = (const char *) handle;
    struct archive *a = open_archive(path);
    struct archive_entry *entry;
    while (archive_read_next_header(a, &entry) == ARCHIVE_OK){
        if (callback(user, archive_entry_pathname(entry)) != BS_OK){
            break;
        }
    }
    close_archive(a);
    return BS_OK;
}

int
arch_copy(void *handle, const char *src, const char *dest)
{
    const char *arch = (const char *) handle;

    char path[PATH_MAX+1];
    char tmpdir[] = ".archiveXXXXXX";
    struct archive *a = open_archive(arch);
    struct archive_entry *entry;
    seek_to_path(a, &entry, src);

    /* make a temp dir */
    if (mkdtemp(tmpdir) == NULL){
        fprintf(stderr, "error: unable to extract file: %s\n", src);
        return BS_ERROR;
    }

    /* save cwd  and extract to tmpdir */
    getcwd(path, PATH_MAX);
    chdir(tmpdir);
    archive_read_extract(a, entry, 0);

    /* move the extracted file to specified location */
    rename(archive_entry_pathname(entry), dest);

    /* cleanup extract directory */
    bs_rmdir(tmpdir);

    return BS_OK;
}

int
arch_load(void *handle, const char *path, uint8_t **data, int64_t *sz)
{
    const char *arch = (const char *) handle;
    struct archive *a = open_archive(arch);
    struct archive_entry *entry;
    int64_t filesize;
    void *buffer;
    int rc;
    seek_to_path(a, &entry, path);

    *data = NULL;
    *sz = 0;

    if (entry == NULL){
        /* FIXME: error handling */
        fprintf(stderr, "error: unable to locate %s in %s\n", path, arch);
        return BS_ERROR;
    }

    /* FIXME: Should check that we can allocate a buffer big enough to
     * store the file in memory:
     *      sysconf SC_AVPHYS_PAGES * SC_PAGESIZE = available memory
     */
    filesize = archive_entry_size(entry);
    buffer = malloc(filesize);
    rc = BS_ERROR;
    if (buffer){
        if (ARCHIVE_OK == archive_read_data(a, buffer, filesize)){
            *data = buffer;
            *sz = filesize;
            rc = BS_OK;
        } else {
            free(buffer);
        }
    }
    close_archive(a);
    return rc;
}

int
arch_exists(void *handle, const char *path, bool *out)
{
    const char *arch = (const char *) handle;
    struct archive *a = open_archive(arch);
    struct archive_entry *entry;
    seek_to_path(a, &entry, path);
    *out = (entry != NULL);
    close_archive(a);
    return BS_OK;
}

int
arch_stat(void *handle, const char *path, BS_Stat *stat)
{
    int rc = BS_ERROR;
    const char *arch = (const char *) handle;
    struct archive *a = open_archive(arch);
    struct archive_entry *entry;
    seek_to_path(a, &entry, path);
    if (entry != NULL){

        stat->mode = archive_entry_mode(entry);
        stat->size = archive_entry_size(entry);
        stat->mtime = archive_entry_mtime(entry);
        stat->atime = archive_entry_atime(entry);
        stat->ctime = archive_entry_ctime(entry);
        stat->uid = archive_entry_uid(entry);
        stat->gid = archive_entry_gid(entry);

        rc = BS_OK;
    }
    close_archive(a);
    return rc;
}

int
arch_extract(void *handle, const char *dir, const char *outfile)
{
    char buf[8192];
    const char *path;
    const char *infile = (const char *) handle;
    struct archive *in, *out;
    struct archive_entry *src, *dst;

    in = open_archive(path);
    out = archive_write_new();
    archive_write_add_filter_xz(out);
    archive_write_set_format_pax_restricted(out);
    archive_write_open_filename(out, outfile);

    while (archive_read_next_header(in, &src) == ARCHIVE_OK){
        path = archive_entry_pathname(src);
        if (bs_startswith(path, dir)){
            dst = archive_entry_new();
            archive_entry_set_pathname(dst, path);
            archive_entry_set_size(dst, archive_entry_size(src));
            archive_entry_set_filetype(dst, archive_entry_filetype(src));
            archive_entry_set_perm(dst, archive_entry_mode(src));
            archive_write_header(out, dst);

            /* while
             *  readon content of file
             *  write to output file
             */
            archive_entry_free(dst);
        }
    }
    archive_write_close(out);
    close_archive(in);
    close_archive(out);
    return BS_OK;
}
