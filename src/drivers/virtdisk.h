#ifndef virtdisk_h
#define virtdisk_h

#include "boyscout.h"

#ifdef __cplusplus
extern "C" {
#endif

void vdi_driver_register(BS *b);
void vdi_driver_free(void *handle);
void *vdi_driver_open(void *handle, const char *path);
int vdi_driver_close(void *handle);
int vdi_iterate(void *handle, void *user, const char *dir, int (*callback)(void *, const char *path));
int vdi_copy(void *handle, const char *src, const char *dest);
int vdi_load(void *handle, const char *path, uint8_t **data, int64_t *sz);
int vdi_exists(void *handle, const char *path, bool *out);
int vdi_stat(void *handle, const char *path, BS_Stat *stat);
int vdi_extract(void *handle, const char *dir, const char *out);

#ifdef __cplusplus
};
#endif


#endif /* virtdisk_h */
