#ifndef arch_h
#define arch_h

#include "boyscout.h"

#ifdef __cplusplus
extern "C" {
#endif

void arch_driver_register(BS *b);
void arch_driver_free(void *handle);
void *arch_driver_open(void *handle, const char *path);
int arch_driver_close(void *handle);
int arch_iterate(void *handle, void *user, const char *dir, int (*callback)(void *, const char *path));
int arch_copy(void *handle, const char *src, const char *dest);
int arch_load(void *handle, const char *path, uint8_t **data, int64_t *sz);
int arch_exists(void *handle, const char *path, bool *out);
int arch_stat(void *handle, const char *path, BS_Stat *stat);
int arch_extract(void *handle, const char *dir, const char *out);

#ifdef __cplusplus
};
#endif


#endif /* arch_h */
