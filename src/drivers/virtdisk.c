#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <limits.h>
#include <guestfs.h>
#include "utils.h"
#include "drivers/virtdisk.h"

typedef struct virtdisk
{
    guestfs_h *g;
    char **file_systems;
}
VirtualDiskImage;

void
vdi_driver_register(BS *b)
{
    /* init guestfs */
    VirtualDiskImage *vdi = malloc(sizeof(VirtualDiskImage));
    vdi->g = guestfs_create_flags(GUESTFS_CREATE_NO_ENVIRONMENT);
    guestfs_set_error_handler(vdi->g, NULL, NULL);
    guestfs_parse_environment(vdi->g);
    vdi->file_systems = NULL;

    /* register */
    bs_backend_register(b,
        "disk_image",
        vdi,
        vdi_driver_free,
        vdi_driver_open,
        vdi_driver_close,
        vdi_iterate,
        vdi_copy,
        vdi_load,
        vdi_exists,
        vdi_stat,
        vdi_extract,
        ".raw", ".qcow2", ".vmdk", ".vdi", ".img", NULL);
}

void
vdi_driver_free(void *handle)
{
    int i;
    VirtualDiskImage *v = (VirtualDiskImage *) handle;
    if (v->g) {
        guestfs_close(v->g);
    }
    free(handle);
}

void *
vdi_driver_open(void *handle, const char *path)
{
    int i;
    VirtualDiskImage *v = (VirtualDiskImage*) handle;
    guestfs_add_drive_ro(v->g, path);
    guestfs_launch(v->g);
    v->file_systems = guestfs_list_filesystems(v->g);

    /* FIXME: There is going to be a lot of cases where this
     * will be broken. However majority of .qcow2 images
     * are using /dev/sda for file system. Need to
     * provide better options around this..
     */
    int rc = -1;
    for (i = 0; v->file_systems[i] != NULL; i+=2){
        if (strstr(v->file_systems[i], "/dev/sda") != NULL){
            rc = guestfs_mount(v->g, v->file_systems[i], "/");
            break;
        }
    }
    if (rc != 0){
        fprintf(stderr, "error: unable to detect mount point for %s.\n", path);
        return NULL;
    }
    return v;
}

int
vdi_driver_close(void *handle)
{
    int i;
    VirtualDiskImage *v = (VirtualDiskImage*) handle;
    if (v->file_systems){
        for (i = 0; v->file_systems[i] != NULL; ++i){
            free(v->file_systems[i]);
        }
        free(v->file_systems);
    }
    if (guestfs_shutdown(v->g) == 0){
        return BS_OK;
    }
    /* FIXME: Error handling */
    return BS_ERROR;
}

int
vdi_iterate(void *handle,
    void *user,
    const char *dir,
    int (*callback)(void *, const char *))
{
    VirtualDiskImage *v = (VirtualDiskImage *) handle;
    char path[PATH_MAX+1];
    char *filename = NULL;
    char tmpfile[] = ".virtdiskXXXXXX";
    FILE *f = fdopen(mkstemp(tmpfile), "r");
    if (dir != NULL){
        guestfs_find0(v->g, dir, tmpfile);
    } else {
        guestfs_find0(v->g, "/", tmpfile);
    }
    while ((filename = bs_read_null_string(f, path)) != NULL){
        if (callback(user, filename) != BS_OK){
            break;
        }
    }
    fclose(f);
    remove(tmpfile);
    return BS_OK;
}

int
vdi_copy(void *handle, const char *src, const char *dest)
{
    VirtualDiskImage *v = (VirtualDiskImage *) handle;
    if (guestfs_download(v->g, src, dest) == 0){
        return BS_OK;
    }
    /* FIXME: error handling */
    return BS_ERROR;
}

int
vdi_load(void *handle, const char *path, uint8_t **data, int64_t *sz)
{
    VirtualDiskImage *v = (VirtualDiskImage *) handle;
    *data = (uint8_t *) guestfs_read_file(v->g, path, (size_t*) sz);
    if (data == NULL){
        /* FIXME: error handling */
        return BS_ERROR;
    }
    return BS_OK;
}

static struct guestfs_statns *
fileinfo(void *handle, const char *path)
{
    VirtualDiskImage *v = (VirtualDiskImage *) handle;
    struct guestfs_statns *file_info;
    file_info = guestfs_statns(v->g, path);
    return file_info;
}

int
vdi_exists(void *handle, const char *path, bool *out)
{
    struct guestfs_statns *file_info = fileinfo(handle, path);
    if (file_info == NULL){
        *out = false;
    } else {
        *out = true;
    }
    guestfs_free_statns(file_info);
    return BS_OK;
}

int
vdi_stat(void *handle, const char *path, BS_Stat *stat)
{
    int rc = BS_ERROR;
    struct guestfs_statns *file_info = fileinfo(handle, path);
    if (file_info){

        stat->mode = file_info->st_mode;
        stat->size = file_info->st_size;
        stat->mtime = file_info->st_mtime_sec;
        stat->atime = file_info->st_atime_sec;
        stat->ctime = file_info->st_ctime_sec;
        stat->uid = file_info->st_uid;
        stat->gid = file_info->st_gid;
        guestfs_free_statns(file_info);
        rc = BS_OK;
    }
    return rc;
}

int
vdi_extract(void *handle, const char *dir, const char *out)
{
    return BS_OK;
}
