#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <dirent.h>
#include <unistd.h>
#include <sys/stat.h>
#include <limits.h>
#include <dlfcn.h>
#include "boyscout.h"
#include "utils.h"

#include "drivers/virtdisk.h"

BS*
bs_init()
{
    BS *b = malloc(sizeof(BS));
    if (sqlite3_open(":memory:", &b->database) != SQLITE_OK ){
        bs_destroy(b);
        free(b);
        return NULL;
    }
    b->plugins = htable_new();
    b->backends = htable_new();
    b->filetype_mappings = htable_new();
    b->commands = htable_new();

    const char *plugin_dir = NULL;
    if ((plugin_dir = getenv("BOYSCOUT_PLUGINS"))){
        bs_plugin_discover(b, plugin_dir);
    }
    return b;
}

void
bs_destroy(BS* b)
{
    if (b->plugins) htable_delete(b->plugins);
    if (b->filetype_mappings) htable_delete(b->filetype_mappings);
    if (b->backends) htable_delete(b->backends);
    if (b->commands) htable_delete(b->commands);
    if (b->database) sqlite3_close(b->database);

    free(b);
    b = NULL;
}

void
bs_plugin_discover(BS *b, const char *plugin_dir)
{
    DIR *dir;
    struct dirent *entry;
    struct stat finfo;
    char plugins[PATH_MAX+1];
    char path[PATH_MAX+1];
    const char *ptr;

    ptr = realpath(plugin_dir, plugins);
    if (!ptr){
        /* FIXME */
        fprintf(stderr, "error: unable to determine absolute path of plugins\n");
        return;
    }
    dir = opendir(plugin_dir);
    while ((entry = readdir(dir)) != NULL){
        snprintf(path, PATH_MAX, "%s/%s", plugins, entry->d_name);
        lstat(path, &finfo);
        if (S_ISREG(finfo.st_mode)){
            bs_plugin_register(b, entry->d_name, path);
        }
    }
    closedir(dir);
}


int
bs_plugin_register(BS *b, const char *name, const char *path)
{
    int rc = BS_ERROR;
    char *str, *saved;
    const char *plugin_name;
    char *func;
    void *lib;
    lib = dlopen(path, RTLD_LAZY);
    if (!lib){
        /* FIXME */
        fprintf(stderr, "error: Not a valid plug-in: %s\n", path);
        return BS_ERROR;
    }
    str = strdup(name);
    plugin_name = strtok_r(str, ".", &saved);
    if (plugin_name){
        asprintf(&func, "%s_register", plugin_name);
        bs_plugin_init init = dlsym(lib, func);
        free(func);
        if (init != NULL){
            if (init(b) == BS_OK){
                htable_put(b->plugins, plugin_name, lib, (HashTableEntryDelete) dlclose);
                rc = BS_OK;
            }
        } else {
            puts(dlerror());
            dlclose(lib);
        }
        free(str);
    }
    return rc;
}

void
bs_backend_destroy(void *ptr)
{
    BS_Backend *driver = (BS_Backend *) ptr;
    driver->destructor(driver->handle);
    free(driver->name);
    free(driver);
}

int
bs_backend_register(BS *b,
        const char *name,
        void *handle,
        bs_backend_destructor destructor,
        bs_backend_open open_driver,
        bs_backend_close close_driver,
        bs_backend_iterate iterate,
        bs_backend_copy copy_file,
        bs_backend_load load_file,
        bs_backend_exists exists,
        bs_backend_stat stat,
        bs_backend_extract extract,
        ...)
{
    char *file_type;
    va_list args;

    BS_Backend *driver = malloc(sizeof(BS_Backend));
    driver->name = strdup(name);
    driver->handle = handle;
    driver->destructor = destructor;
    driver->open_driver = open_driver;
    driver->close_driver = close_driver;
    driver->iterate = iterate;
    driver->copy_file = copy_file;
    driver->load_file = load_file;
    driver->exists = exists;
    driver->stat = stat;
    driver->extract = extract;

    htable_put(b->backends, name, driver, bs_backend_destroy);
    va_start(args, extract);
    while ((file_type = va_arg(args, char *)) != 0){
        htable_put(b->filetype_mappings, file_type, driver, NULL);
    }
    va_end(args);

    return BS_OK;
}

static bool
display_driver(HashTableEntry *entry, size_t idx, void *data)
{
    BS_Backend *driver = (BS_Backend *) entry->value;
    printf("%s =>  %s\n", entry->key, driver->name);
    return true;
}

int
bs_supported_files(BS *b)
{
    htable_enumerate(b->filetype_mappings, NULL, display_driver);
    return BS_OK;
}

void
bs_command_destroy(void *ptr)
{
    BS_Command *c = (BS_Command*) ptr;
    free(c->name);
    free(c->help);
    free(c);
}

int
bs_command_register(BS *b,
        const char *name,
        int (*cmd)(BS *b, int argc, const char **argv),
        const char *help)
{
    BS_Command *c = malloc(sizeof(BS_Command));
    c->name = strdup(name);
    c->command = cmd;
    c->help = strdup(help);

    htable_put(b->commands, name, c, bs_command_destroy);
    return BS_OK;
}

int
bs_database_prepared(BS *b,
        int (*callback)(void*,int,char**,char**),
        void *user_data,
        const char *sql,
        const char *bind_fmt,
        ...)
{
    int rc, row = 0, cols, i, n;
    double d;
    char **values, **names, *s;
    va_list args;
    sqlite3_stmt *stmt;

    sqlite3_prepare(b->database, sql, strlen(sql), &stmt, NULL);
    va_start(args, bind_fmt);
    i = 1;
    while (*bind_fmt){

        switch (*bind_fmt++){

            /* bind text */
            case 's':
                s = va_arg(args, char *);
                sqlite3_bind_text(stmt, i, s, strlen(s), SQLITE_STATIC);
                break;

            /* bind integer */
            case 'i':
                n = va_arg(args, int);
                sqlite3_bind_int(stmt, i, n);
                break;

            /* bind double */
            case 'd':
                d = va_arg(args, double);
                sqlite3_bind_double(stmt, i, d);
                break;

            /*  unsupported */
            default:
                sqlite3_finalize(stmt);
                va_end(args);
                return BS_ERROR;
        }
        ++i;
    }
    va_end(args);

    /* iterate through results and send them to callback */
    while ((rc = sqlite3_step(stmt)) == SQLITE_ROW){
        printf("rc = %d\n", rc);
        if (callback != NULL){
            cols = sqlite3_column_count(stmt);
            values = malloc(cols * sizeof(char *));
            names = malloc(cols * sizeof(char *));
            for (i = 0; i < cols; ++i){
                values[i] = (char *)sqlite3_column_text(stmt, i);
                names[i] =  (char *)sqlite3_column_name(stmt, i);
            }
            callback(user_data, cols, values, names);
            free(values);
            free(names);
        }
    }
    if (rc != SQLITE_DONE){
        fprintf(stderr, "error: %s\n", sqlite3_errmsg(b->database));
    }
    sqlite3_finalize(stmt);
    return BS_OK;
}

int
bs_database_exec(BS *b,
        int (*callback)(void *, int, char **, char **),
        void *user_data,
        const char *sql)
{
    int rc;
    char *err;
    rc = sqlite3_exec(b->database, sql, callback, user_data, &err);
    if (rc != SQLITE_OK){
        /* FIXME */
        fprintf(stderr, "error: %s\n", err);
        sqlite3_free(err);
        return BS_ERROR;
    }
    return BS_OK;
}

void
bs_file_destroy(void *ptr)
{
    BS_File *f = (BS_File *) ptr;
    ((BS_Backend*) f->driver)->close_driver(f->handle);
    free(f->path);
    free(f);
    f = NULL;
}

struct chk_extension_args {
    const char *path;
    const char *key;
};

static bool
chk_extension(HashTableEntry *entry, size_t idx, void *data){
    struct chk_extension_args *args = data;
    bool found = bs_endswith(args->path, entry->key);
    if (found){
        args->key = entry->key;
    }
    return ! found;
}

int
bs_open(BS *b, const char *path, BS_File **f)
{
    char *digest;
    const char *extension;
    BS_File *file;
    HashTableEntry *entry;

    if (! bs_local_file_exists(path)){
        /* FIXME */
        fprintf(stderr, "error: path not found '%s'\n", path);
        return BS_ERROR;
    }

    /*
     * FIXME: This is pretty terrible. Should really be able to lookup hashtable
     * by extension, however things get complicated with the .tar.gz
     * foor.bar.tar cases.
     */
    struct chk_extension_args args;
    args.path = path;
    args.key = NULL;
    htable_enumerate(b->filetype_mappings, &args, chk_extension);
    if (args.key == NULL){
        /* FIXME */
        fprintf(stderr, "error: unable to determine file type for: %s\n", path);
        return BS_ERROR;
    }

    entry = htable_get(b->filetype_mappings, args.key);
    if (entry){
        file = malloc(sizeof(BS_File));
        file->path = strdup(path);
        BS_Backend *drv = (BS_Backend *) entry->value;
        file->driver = drv;
        file->handle = drv->open_driver(drv->handle, path);
        *f = file;
        return BS_OK;
    }

    /* FIXME */
    fprintf(stderr, "error: compatible driver not detected for: %s\n", path);
    return BS_ERROR;
}

int
bs_close(BS *b, BS_File *f)
{
    ((BS_Backend*) f->driver)->close_driver(f->handle);
    free(f->path);
    free(f);
    f = NULL;
    return BS_OK;
}

int
bs_iterate(BS *b,
        BS_File *f,
        void *user,
        const char *dir,
        int (*callback)(void *, const char *))
{

    BS_Backend* driver = (BS_Backend*) f->driver;
    return driver->iterate(f->handle, user, dir, callback);
}

int
bs_file_copy(BS *b, BS_File *f, const char *src, const char *dest)
{
    BS_Backend* driver = (BS_Backend*) f->driver;
    return driver->copy_file(f->handle, src, dest);
}

int
bs_file_load(BS *b, BS_File *f, const char *src, uint8_t **dest, int64_t *size)
{
    BS_Backend* driver = (BS_Backend*) f->driver;
    return driver->load_file(f->handle, src, dest, size);
}

int
bs_file_exists(BS *b, BS_File *f, const char *path, bool *out)
{
    BS_Backend* driver = (BS_Backend*) f->driver;
    return driver->exists(f->handle, path, out);
}

int
bs_file_stat(BS *b, BS_File *f, const char *path, BS_Stat *stat)
{
    BS_Backend *driver = (BS_Backend *) f->driver;
    return driver->stat(f->handle, path, stat);
}

int
bs_extract(BS *b, BS_File *f, const char *dir, const char *out)
{
    BS_Backend *driver = (BS_Backend *) f->driver;
    return driver->extract(f->handle, dir, out);
}
