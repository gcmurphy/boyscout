#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include <dirent.h>
#include <sys/stat.h>
#include <pwd.h>
#include <nettle/sha1.h>
#include "utils.h"

char*
bs_hash(const char *data, size_t size)
{
    int i, j;
    size_t out;
    uint8_t digest[SHA1_DIGEST_SIZE];
    char *hexdigest = NULL;
    struct sha1_ctx ctx;
    sha1_init(&ctx);
    sha1_update(&ctx, size, (const uint8_t*)data);
    sha1_digest(&ctx, sizeof(digest), digest);

    out = SHA1_DIGEST_SIZE * 2;
    hexdigest = malloc(out + 1);
    for (i = 0, j = 0; i < SHA1_DIGEST_SIZE; ++i, j+=2){
        sprintf(hexdigest+j, "%02x", digest[i]);
    }
    hexdigest[out] = '\0';
    return hexdigest;
}

const char*
bs_file_extension(const char *path)
{
    return strrchr(path, '.');
}

bool
bs_endswith(const char *str, const char *suffix)
{
    const char *s = str + strlen(str);
    const char *cmp = suffix + strlen(suffix);

    if (strlen(str) < strlen(suffix)){
        return false;
    }

    while (cmp > suffix && *cmp == *s) { cmp--; s--; }
    return *cmp == *s;
}

bool
bs_startswith(const char *str, const char *prefix)
{
    size_t slen = strlen(str);
    size_t plen = strlen(prefix);

    if (plen > slen){
        return false;
    }

    return strncmp(str, prefix, plen) == 0;
}

const char*
bs_get_home_directory()
{
    const char *homedir = getenv("HOME");
    if (homedir == NULL){
        homedir = getpwuid(getuid())->pw_dir;
    }
    return homedir;
}

void
bs_rmdir(const char *path)
{

    DIR *d;
    struct dirent *entry;
    char filename[PATH_MAX+1];
    struct stat fileinfo;

    d = opendir(path);
    while ( d && (entry = readdir(d)) != NULL){
        if (strcmp(".", entry->d_name) == 0 || strcmp("..", entry->d_name) == 0) {
            continue;
        }
        snprintf(filename, PATH_MAX, "%s/%s", path, entry->d_name);
        stat(filename, &fileinfo);
        if (S_ISDIR(fileinfo.st_mode)){
            bs_rmdir(filename);
        } else {
            unlink(filename);
        }
    }
    closedir(d);
    rmdir(path);
}

const char*
bs_get_editor()
{
    const char *editor = getenv("EDITOR");
    if (editor == NULL){
        editor = "/usr/bin/vim";
    }
    return editor;
}

char*
bs_read_null_string(FILE *f, char *out)
{
    char c;
    int i;

    out[0] = '/';
    i = 1;
    while ((c = fgetc(f)) != EOF){
        out[i++] = c;
        if (c == '\0'){
            return out;
        }
    }
    return NULL;
}

char *
bs_permission_string(int64_t mode)
{
    char *perms;
    const char *d, *ur, *uw, *ux,
        *gr, *gw, *gx, *or, *ow, *ox;

    /* get file permissions */
    d = (mode & S_IFDIR) ? "d" : "-";
    ur = (mode & S_IRUSR) ? "r" : "-";
    uw = (mode & S_IWUSR) ? "w" : "-";
    ux = (mode & S_IXUSR) ? "x" : "-";
    gr = (mode & S_IRGRP) ? "r" : "-";
    gw = (mode & S_IWGRP) ? "w" : "-";
    gx = (mode & S_IXGRP) ? "x" : "-";
    or = (mode & S_IROTH) ? "r" : "-";
    ow = (mode & S_IWOTH) ? "w" : "-";
    ox = (mode & S_IXOTH) ? "x" : "-";
    asprintf(&perms, "%s%s%s%s%s%s%s%s%s%s",
            d, ur, uw, ux, gr, gw, gx, or, ow, ox);

    return perms;
}

bool
bs_local_file_exists(const char *path)
{
    FILE *f = fopen(path, "r");
    if (f == NULL){
        return false;
    }
    fclose(f);
    return true;
}
