#ifndef chain_h
#define chain_h

#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef void (*LinkValueDelete)(void *);

typedef struct link {
    struct link *next;
    struct link *prev;
    void *value;
    LinkValueDelete release;
} Link;

typedef bool (*LinkVisitor)(Link *, void *);
typedef bool (*LinkComparator)(void *, void *);

typedef struct chain {
    Link *head;
    Link *tail;
    size_t count;
} Chain;


Chain *chain_new();
void chain_delete(Chain *chain);
void chain_push_back(Chain *chain, void *value, LinkValueDelete release);
void chain_push_front(Chain *chain, void *value, LinkValueDelete release);
void* chain_pop_back(Chain *chain);
void* chain_pop_front(Chain *chain);
size_t chain_size(Chain *chain);
Link *chain_find(Chain *chain, LinkComparator cmp, void *user);
Link *chain_iter_forward(Chain *chain, LinkVisitor visitor, void *user);
Link *chain_iter_back(Chain *chain, LinkVisitor visitor, void *user);
void chain_unlink(Chain *chain, Link *link);

#ifdef __cplusplus
};
#endif

#endif /* chain_h */
