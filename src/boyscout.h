#ifndef boyscout_h
#define boyscout_h

#include <stdint.h>
#include <sqlite3.h>
#include "ht.h"

#define BS_ERROR   -1
#define BS_OK       0
#define BS_EXIT     1

#ifdef __cplusplus
extern "C" {
#endif

typedef struct bs_stat {
    int64_t mode;
    int64_t size;
    int64_t mtime;
    int64_t ctime;
    int64_t atime;
    int64_t uid;
    int64_t gid;
} BS_Stat;

typedef void (*bs_backend_destructor)(void *handle);
typedef void *(*bs_backend_open)(void *handle, const char *path);
typedef int (*bs_backend_close)(void *handle);
typedef int (*bs_backend_iterate)(void *handle, void *user, const char *dir, int (*callback)(void *, const char *path));
typedef int (*bs_backend_copy)(void *handle, const char *src, const char *dest);
typedef int (*bs_backend_load)(void *handle, const char *path, uint8_t **data, int64_t *size);
typedef int (*bs_backend_exists)(void *handle, const char *path, bool *out);
typedef int (*bs_backend_stat)(void *handle, const char *path, BS_Stat *stat);
typedef int (*bs_backend_extract)(void *handle, const char *dir, const char *txz);

typedef struct bs_backend {
    void *handle;
    char *name;
    bs_backend_destructor destructor;
    bs_backend_open open_driver;
    bs_backend_close close_driver;
    bs_backend_iterate iterate;
    bs_backend_copy copy_file;
    bs_backend_load load_file;
    bs_backend_exists exists;
    bs_backend_stat stat;
    bs_backend_extract extract;
}
BS_Backend;

typedef struct bs  {
    HashTable *plugins;
    HashTable *backends;
    HashTable *filetype_mappings;
    HashTable *commands;
    sqlite3 *database;
}
BS;

typedef struct bs_file_context
{
    char *path;
    void *handle;
    void *driver;
}
BS_File;

typedef struct bs_command {
    int (*command)(BS *b, int argc, const char *argv[]);
    char *name;
    char *help;
} BS_Command;

typedef int (*bs_plugin_init)(void *context);

BS* bs_init();

void bs_destroy(BS* b);

int bs_supported_files(BS *b);
int bs_plugin_register(BS *b, const char *name, const char *path);
void bs_plugin_discover(BS *b, const char *plugin_dir);

int
bs_backend_register(BS *b,
        const char *name,
        void *handle,
        bs_backend_destructor destructor,
        bs_backend_open open_driver,
        bs_backend_close close_driver,
        bs_backend_iterate iterate,
        bs_backend_copy copy_file,
        bs_backend_load load_file,
        bs_backend_exists exists,
        bs_backend_stat stat,
        bs_backend_extract extract,
        ...);

int bs_command_register(BS *b,
                        const char *name,
                        int (*command)(BS *, int, const char **),
                        const char *help);


int bs_database_prepared(BS *b,
    int (*callback)(void*,int,char**,char**),
    void *user_data,
    const char *sql,
    const char *bind_fmt,
    ...);

int bs_database_exec(BS *b,
    int (*callback)(void *, int, char **, char **),
    void *user_data,
    const char *sql);

int bs_open(BS *b, const char *path, BS_File **f);
int bs_close(BS *b, BS_File *f);
int bs_iterate(BS *b, BS_File *f, void *user, const char *dir, int (*callback)(void *, const
char *));

int bs_file_copy(BS *b, BS_File *f, const char *src, const char *dest);
int bs_file_load(BS *b, BS_File *f, const char *src, uint8_t **dest, int64_t *size);
int bs_file_exists(BS *b, BS_File *f, const char *path, bool *out);
int bs_file_stat(BS *b, BS_File *f, const char *path, BS_Stat *stat);
int bs_extract(BS *b, BS_File *f, const char *dir, const char *out);

#ifdef __cplusplus
};
#endif

#endif /* boyscout_h */
