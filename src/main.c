#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <argp.h>
#include "boyscout.h"

const char *argp_program_version = "boyscout 0.1-alpha";
const char *argp_program_bug_address = "grant.murphy@hp.com";
static char doc[] = "boyscout -- discover information about virtual disk image";

static struct argp_option options[] = {
    { "image", 'i', "FILE", 0, "Image file to mount and inspect." },
    { 0 }
};

struct arguments {
    char *image_file;
};

static error_t parse_opt(int opt, char *arg, struct argp_state *state)
{
    struct arguments *args = state->input;
    switch(opt) {
        case 'i':
            args->image_file = arg;
            break;
        default:
            return ARGP_ERR_UNKNOWN;
    }
    return 0;
}

static struct argp argp = { options, parse_opt, NULL, doc };

void scout(const char *path){
    BS *b = bs_init();

    bs_destroy(b);
}

int main(int argc, char *argv[]){

    struct arguments args;
    args.image_file = NULL;
    argp_parse(&argp, argc, argv, 0, 0, &args);
    if (! args.image_file ){
        fprintf(stderr, "error: required option --image missing.\n");
        exit(2);
    }
    if (access(args.image_file, R_OK) != 0){
        fprintf(stderr, "Cannot access file: %s.\n", args.image_file);
        exit(2);
    }
    scout(args.image_file);
    exit(0);
}
