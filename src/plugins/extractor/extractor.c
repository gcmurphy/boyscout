#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <sys/stat.h>
#include <limits.h>
#include "boyscout.h"
#include "utils.h"
#include "plugins/extractor/extractor.h"

static char create_extracted_files_table[] =
    "CREATE TABLE IF NOT EXISTS 'extracted_files' ("
    "   input_file TEXT,  "
    "   output_file TEXT, "
    "   sha1 TEXT"
    ")";

static char insert_into_extracted_files[] =
    "INSERT INTO 'extracted_files' ("
    "   input_file,  "
    "   output_file,"
    "   sha1)        "
    "VALUES (?, ?)";

static int
extract_file(BS *b, BS_File *f, const char *src, const char *dest)
{
    int rc;
    uint8_t *buf;
    int64_t buf_sz;
    char *hash = NULL;
    char path[PATH_MAX];

    BS_Stat fileinfo;
    memset(&fileinfo, 0, sizeof(BS_Stat));

    rc = bs_file_stat(b, f, src, &fileinfo);
    if (rc != BS_OK){
        return rc;
    }

    bs_file_load(b, f, path, &buf, &buf_sz);
    hash = bs_hash((const char *) buf, (size_t) buf_sz);
    snprintf(path, sizeof(path), "%s/%s", dest, hash);
    FILE *out = fopen(path, "wb");
    if (out){
        fwrite(buf, 1, buf_sz, out);
        fclose(out);
    }

    bs_database_prepared(b, NULL, NULL,
            insert_into_extracted_files, "sss", src, path, hash);

    free(buf);
    free(hash);
    return BS_OK;
}

static int
extractor_run(BS *b, int argc, const char *argv[])
{
    int rc, i;
    BS_File *f = NULL;

    if (argc != 3){
        return BS_ERROR;
    }

    rc = bs_open(b, argv[0], &f);
    if (rc != BS_OK){
        return BS_ERROR;
    }

    rc = extract_file(b, f, argv[1], argv[2]);
    bs_close(b, f);
    return rc;
}

int
extractor_register(void *context)
{
    BS *b = (BS *) context;
    int rc = bs_database_exec(b, NULL, NULL, create_extracted_files_table);
    if (rc != BS_OK){
        /* FIXME: error reporting */
        fprintf(stderr, "error: unable to create table for storing plugin data\n");
        return rc;
    }
    rc = bs_command_register(b, "extract", extractor_run,
        "Extract a file from the supplied image");

    return rc;
}
