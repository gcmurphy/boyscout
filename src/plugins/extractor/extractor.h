#ifndef extractor_h
#define extractor_h
#ifdef __cplusplus
extern "C" {
#endif

#if __GNUC__ >= 4
    #define DLL_PUBLIC __attribute__((visibility("default")))
#else
    #define DLL_PUBLIC
#endif


DLL_PUBLIC int extractor_register(void *context);

#ifdef __cplusplus
};
#endif
#endif /* extractor_h */
