#ifndef fileinfo_h
#define fileinfo_h
#ifdef __cplusplus
extern "C" {
#endif

#if __GNUC__ >= 4
    #define DLL_PUBLIC __attribute__((visibility("default")))
#else
    #define DLL_PUBLIC
#endif


DLL_PUBLIC int fileinfo_register(void *context);

#ifdef __cplusplus
};
#endif
#endif /* fileinfo_h */
