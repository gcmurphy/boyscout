#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <sys/stat.h>
#include "boyscout.h"
#include "utils.h"
#include "plugins/fileinfo/fileinfo.h"

static char create_fileinfo_table[] =
    "CREATE TABLE IF NOT EXISTS 'file_info' ("
    "   path TEXT,  "
    "   uid BIGINT, "
    "   gid BIGINT, "
    "   mode TEXT,  "
    "   size BIGINT,"
    "   atime BIGINT,"
    "   mtime BIGINT,"
    "   ctime BIGINT,"
    "   sha1 TEXT"
    ")";

static char insert_into_fileinfo[] =
    "INSERT INTO 'file_info' ("
    "   path,   "
    "   uid,    "
    "   gid,    "
    "   mode,   "
    "   size,   "
    "   atime,  "
    "   mtime,  "
    "   ctime,  "
    "   sha1)   "
    "   VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";

struct fileinfo_args {
    BS *b;
    BS_File *f;
};

static bool
fileinfo_blacklisted(const char *path)
{
    int rule;
    const char *blacklisted[] = {
        "/dev",
        NULL
    };

    for (rule = 0; blacklisted[rule] != NULL; ++rule){
        if (strncmp(blacklisted[rule], path, strlen(blacklisted[rule])) == 0){
            return true;
        }
    }
    return false;
}

static int
fileinfo_store_file(void *handle, const char *path)
{
    char *perms, *hash;
    uint8_t *buf;
    int64_t buf_sz;
    struct fileinfo_args *ctx;

    ctx = (struct fileinfo_args *) handle;
    BS_Stat fileinfo;
    memset(&fileinfo, 0, sizeof(BS_Stat));
    bs_file_stat(ctx->b, ctx->f, path, &fileinfo);

    /* skip blacklisted (e.g. devices) and directories */
    if (! S_ISREG(fileinfo.mode) || fileinfo_blacklisted(path)){
        /* FIXME: Add debug output */
        return BS_OK;
    }
    /* convert file permissions */
    perms = bs_permission_string(fileinfo.mode);

    /* compute sha1 for everything not in /dev */
    bs_file_load(ctx->b, ctx->f, path, &buf, &buf_sz);
    hash = bs_hash((const char *) buf, (size_t) buf_sz);

    /* store in database */
    bs_database_prepared(ctx->b, NULL, NULL, insert_into_fileinfo,
        "siisiiiis", path, fileinfo.uid, fileinfo.gid,
        perms, fileinfo.size, fileinfo.atime, fileinfo.mtime,
        fileinfo.ctime, hash);

    free(buf);
    free(hash);
    free(perms);
    return BS_OK;
}

static int
fileinfo_run(BS *b, int argc, const char *argv[])
{
    int rc, i;
    BS_File *f = NULL;
    struct fileinfo_args args;

    for (i = 0; i < argc; ++i){
        rc = bs_open(b, argv[i], &f);
        if (rc == BS_OK) {
            args.f = f;
            args.b = b;
            bs_database_exec(b, NULL, NULL, "BEGIN TRANSACTION");
            bs_iterate(b, f, &args, "/", fileinfo_store_file);
            bs_database_exec(b, NULL, NULL, "COMMIT TRANSACTION");
            bs_close(b, f);
            f = NULL;
        }
    }

    return BS_OK;
}

int
fileinfo_register(void *context)
{
    BS *b = (BS *) context;
    int rc = bs_database_exec(b, NULL, NULL, create_fileinfo_table);
    if (rc != BS_OK){
        /* FIXME: error reporting */
        fprintf(stderr, "error: unable to create table for storing plugin data\n");
        return rc;
    }
    rc = bs_command_register(b, "fileinfo", fileinfo_run,
        "Create a record and hash for ever file within the supplied file");

    return rc;
}
